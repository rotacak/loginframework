<?php
/*
 * Login Framework
 *
 * Features:
 * - login
 * - logout
 * - register
 * - forgotten password
 */
class LoginFramework {
    public $email;          // Email class
    public $msg;            // Message class
    private $post;          // $_POST array (by default)
    private $get;           // $_GET array (by default)
    private $db;            // PDO instance

    function __construct($config = null) {
        // Optional config passed into class
        if (!is_null($config))
            LfConfig::setArray ($config);

        $this->msg = new LfMessage();

        $this->email = new LfEmail();
        $this->email->msg = $this->msg; //TODO static class
    }

    /**
     * Return value of input key from form
     *
     * @Return string
     */
    public function getInputValue($inputKey) {
        return $this->post[LfConfig::get($inputKey)];
    }
    
    /**
     * Run configured framework
     * 
     * @return *|array - Response
     */
    public function run() {
        // Store data into variable
        $this->post = LfConfig::get('_POST');
        $this->get = LfConfig::get('_GET');

        // Proccess GET
        if (isset($this->get[LfConfig::get('FORGOTTEN_DB_COLUMN_HASH')]))
            return $this->forgottenPasswordChangeFormProcess();

        // Proccess POST
        if (!isset($this->post[LfConfig::get('INPUT_TYPE')]))
            return;

        $inputType = $this->getInputValue('INPUT_TYPE');
        if (!isset($inputType))
            return;

        if ($inputType == 'login')
            return $this->loginFormProcess();
        else if ($inputType == 'register')
            return $this->registerFormProcess();
        else if ($inputType == 'forgottenPasswordRequest')
            return $this->forgottenPasswordRequestFormProcess();
        else if ($inputType == 'forgottenPasswordChange')
            return $this->forgottenPasswordChangeFormProcess();

        // Type of form was not catched
        $this->msg->setError(LfText::get('WrongInputType'), 'Wrong input type', [
            'INPUT_TYPE value' => $this->getInputValue('INPUT_TYPE')
        ]);

        return $this->msg->getError();
    }

    /**
     * Return PDO object for any use
     *
     * @return PDO|array - PDO object or array with errors
     */
    public function getPDO() {
        $pdo = $this->db;

        // We didn't used run() yet so we connect to database manually
        if (is_null($pdo))
            $pdo = $this->databaseConnect();

        // We failed to connect database manually so return error array
        if (is_null($pdo))
            return $this->msg->getError();
        else
            return $pdo;
    }

    /**
     * Process login form
     * 
     * @return array - Response
     */
    private function loginFormProcess() {
        $this->loginFormValidate();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        $this->db = $this->databaseConnect();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        if (!is_null($this->db))
            $this->login();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        return $this->msg->getSuccess();
    }

    /**
     * Process register form
     *
     * @return array - Response
     */
    private function registerFormProcess() {
        $this->registerFormValidate();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        $this->db = $this->databaseConnect();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        if (!is_null($this->db)) // TODO remove?
            $this->register();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        return $this->msg->getSuccess();
    }

    /**
     * Process forgotten password request form
     *
     * @return array - Response
     */
    private function forgottenPasswordRequestFormProcess() {
        $this->forgottenPasswordRequestFormValidate();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        $this->db = $this->databaseConnect();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        if (!is_null($this->db)) // TODO remove?
            $this->forgottenPasswordRequest();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        return $this->msg->getSuccess();
    }

    /**
     * Process forgotten password request form
     *
     * @return array - Response
     */
    private function forgottenPasswordChangeFormProcess() {
        $this->forgottenPasswordChangeFormValidate();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        $this->db = $this->databaseConnect();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        if (!is_null($this->db)) // TODO remove?
            $this->forgottenPasswordChange();

        if ($this->msg->isError()) // TODO duplicity
            return $this->msg->getError();

        return $this->msg->getSuccess();
    }

    /**
     * Log user in
     *
     * @return array|boolean
     */
    private function login() {
        // Prepare list of fetched columns for success response
        $customColumnsSql = '';
        $customColumnsArray = LfConfig::get('LOGIN_FETCH_COLUMNS');

        if (!is_null($customColumnsArray)) {
            $customColumnsSql = ', '.implode($customColumnsArray, ', ');
        }
        
        // Find user by login
        $stmt = $this->db->prepare('SELECT
                                        '.LfConfig::get('DB_COLUMN_PASSWORD').'
                                        '.$customColumnsSql.'
                                    FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                    WHERE '.LfConfig::get('DB_COLUMN_LOGIN').'=:login
                                    ');

        $stmt->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);

        $result = $stmt->execute();

        // All results (if is correct then is equal to 1 - one record with this login)
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Take just one row (if is correct then there is only one row)
        if (isset($rows[0]))
            $row = $rows[0];

        // Number of returned rows for error detecting
        $rowCount = count($rows);

        if (!$result) {
            $this->msg->setError(LfText::get('LoginFailed'), 'SQL error.', 'Query: '.$stmt->queryString);

            return;
        }

        if ($rowCount != 1) {
            $this->msg->setError(LfText::get('LoginFailed'),
                    'Wrong login',
                    [
                        'login' =>
                        [
                            'DB_COLUMN_LOGIN' => LfConfig::get('DB_COLUMN_LOGIN'),
                            'INPUT_LOGIN value' => $this->getInputValue('INPUT_LOGIN')
                        ],
                        'row_count' => $rowCount,
                        'sql' => $stmt->queryString
                    ]);

            return;
        }

        if (!$this->comparePassword($this->getInputValue('INPUT_PASSWORD'), $row[LfConfig::get('DB_COLUMN_PASSWORD')], LfConfig::get('AUTH_TYPE'), $row)) {
            $AUTH_TYPE = LfConfig::get('AUTH_TYPE');
            $passwordValue = $this->getInputValue('INPUT_PASSWORD');

            $this->msg->setError(LfText::get('LoginFailed'), 'Wrong password', [
                    'INPUT_LOGIN value' => $this->getInputValue('INPUT_LOGIN'),
                    'INPUT_PASSWORD value' => $passwordValue.($AUTH_TYPE['md5'] === true ? ' (md5 hash: '.md5($passwordValue).')' : ''),
                    'DB_COLUMN_PASSWORD value' => $row[LfConfig::get('DB_COLUMN_PASSWORD')],
                    'AUTH_TYPE' => $AUTH_TYPE
                ]);

            return;
        }

        // Success response
        $this->msg->setSuccess(LfText::get('LoginSuccess'), $this->removePasswordFromArray($customColumnsArray, $row));
    }

    /**
     * Register user in
     *
     * @return array|boolean
     */
    private function register() {
        // Prepare list of fetched columns for success response
        $customColumnsSql = '';
        $customColumnsArray = LfConfig::get('REGISTER_FETCH_COLUMNS');

        if (!is_null($customColumnsArray)) {
            $customColumnsSql = ', '.implode($customColumnsArray, ',');
        }

        // Find user by login
        $stmt = $this->db->prepare('SELECT
                                        '.LfConfig::get('DB_COLUMN_PASSWORD').'
                                    FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                    WHERE '.LfConfig::get('DB_COLUMN_LOGIN').'=:login
                                    ');

        $stmt->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);

        $result = $stmt->execute();

        // All results (if is correct then is equal to 0 - no records with this login)
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$result) {
            $this->msg->setError(LfText::get('RegisterFailed'), 'SQL error.', 'Query: '.$stmt->queryString);

            return;
        }

        if (count($rows) != 0) {
            $this->msg->setError(LfText::get('RegisterFailed'),
                    'Login "'.$this->getInputValue('INPUT_LOGIN').'" is already registered',
                    [
                        'DB_COLUMN_LOGIN' => LfConfig::get('DB_COLUMN_LOGIN'),
                        'INPUT_LOGIN value' => $this->getInputValue('INPUT_LOGIN')
                    ]);

            return;
        }

        // Prepare custom values for inserting also in database, no validation, just as is
        $saveInputs = LfConfig::get('REGISTER_SAVE_INPUTS');
        $saveInputsColumns = '';
        $saveInputsValues = '';
        $uniqueColumnsArr = [];

        if (!is_null($saveInputs)) {
            foreach ($saveInputs as $key => $value) {
                $saveInputsColumns .= ','.$key;
                $saveInputsValues .= ',:'.$key;

                if ($value === 'unique')
                    $uniqueColumnsArr[] = $key;
            }
        }

        // Do we need save custom and unique data to database?
        if (count($uniqueColumnsArr) > 0) {
            $uniqueResult = $this->registerCheckUniqueUserData($uniqueColumnsArr);

            // JSON response is set inside function (for sql error)
            if ($uniqueResult === false)
                return;

            if ($uniqueResult !== true) {
                $this->msg->setError(LfText::get('RegisterFailed'), $uniqueResult);

                return;
            }
        }

        // Encode password
        $password = $this->encodePassword($this->getInputValue('INPUT_PASSWORD'));

        $stmt2 = $this->db->prepare('INSERT INTO `'.LfConfig::get('DB_TABLE_USER').'`
                                    (
                                        '.LfConfig::get('DB_COLUMN_LOGIN').',
                                        '.LfConfig::get('DB_COLUMN_PASSWORD').'
                                        '.$saveInputsColumns.'
                                    )
                                    VALUES
                                    (
                                        :login,
                                        :password
                                        '.$saveInputsValues.'
                                    )
                                    ');

        if (!is_null($saveInputs)) {
            foreach ($saveInputs as $key => $value) {
                $stmt2->bindValue(':'.$key, $this->post[$key], PDO::PARAM_STR);
            }
        }
        $stmt2->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);
        $stmt2->bindValue(':password', $password, PDO::PARAM_STR);

        $result2 = $stmt2->execute();

        if (!$result2) {
            $this->msg->setError(LfText::get('RegisterFailed'), 'SQL error.', 'Query: '.$stmt2->queryString);

            return;
        }

        if ($stmt2->rowCount() != 1) {
            $this->msg->setError(LfText::get('RegisterFailed'),
                    'User was not inserted into database',
                    [
                        'sql' => $stmt->queryString
                    ]);

            return;
        }

        // Get data from new user
        if (!is_null(LfConfig::get('DB_COLUMN_ID'))) {
            $stmt3 = $this->db->prepare('SELECT
                                    '.LfConfig::get('DB_COLUMN_ID').'
                                    '.$saveInputsColumns.'
                                    '.$customColumnsSql.'
                                FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                WHERE '.LfConfig::get('DB_COLUMN_LOGIN').'=:login
                                ');

            $stmt3->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);

            $result3 = $stmt3->execute();

            $row = $stmt3->fetch(PDO::FETCH_ASSOC);

            if (!$result3)
                $this->msg->setError(LfText::get('RegisterSuccessButSomeErrors'), 'SQL error.', 'Query: '.$stmt3->queryString);
            else
                $newId = $row[LfConfig::get('DB_COLUMN_ID')];
        }

        // Send email?
        if (LfConfig::get('REGISTER_SEND_EMAIL_AUTOMATICALLY') === true)
            $this->email->send(LfConfig::get('REGISTER_EMAIL_TEMPLATE_FILE'), $row, 'Registration complete');

        // Response array
        $responseArray = [];
        $responseArray['user_id'] =  isset($newId) ? $newId : ''; // TODO: why sometime not returning id? Make it as fetch column but use this only for forgotten email etc. but not for returning id

        if (isset($row)) {
            // Custom saved inputs
            if (!is_null($saveInputs)) {
                foreach ($saveInputs as $key => $value) {
                    $responseArray[$key] = $row[$key];
                }
            }

            // Custom fetch inputs
            if (!is_null($customColumnsArray)) {
                foreach ($customColumnsArray as $value) {
                    $responseArray[$value] = $row[$value];
                }
            }
        }

        // Success response
        $this->msg->setSuccess(LfText::get('RegisterSuccess'), $responseArray);
    }

    /**
     * Forgotten password request
     *
     * @return array|boolean
     */
    private function forgottenPasswordRequest() {
        // Prepare list of fetched columns for template
        $customColumnsSql = '';
        $customColumnsArray = LfConfig::get('FORGOTTEN_FETCH_COLUMNS');

        if (!is_null($customColumnsArray)) {
            $customColumnsSql = ', '.implode($customColumnsArray, ', ');
        }

        // Find user by login
        $stmt = $this->db->prepare('SELECT 
                                        '.LfConfig::get('DB_COLUMN_ID').',
                                        '.LfConfig::get('DB_COLUMN_EMAIL').',
                                        '.LfConfig::get('DB_COLUMN_LOGIN').'
                                        '.$customColumnsSql.'
                                    FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                    WHERE '.LfConfig::get('DB_COLUMN_LOGIN').'=:login
                                    ');

        $stmt->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);

        $result = $stmt->execute();

        // todo: not work
        if (!$result) {
            $this->msg->setError(LfText::get('ForgottenPasswordFailed'), 'SQL error.', 'Query: '.$stmt->queryString);

            return;
        }

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row === false) {
            $this->msg->setError(LfText::get('ForgottenPasswordFailed'),
                    'Login "'.$this->getInputValue('INPUT_LOGIN').'" not found.',
                    [
                        'DB_COLUMN_LOGIN' => LfConfig::get('DB_COLUMN_LOGIN'),
                        'INPUT_LOGIN value' => $this->getInputValue('INPUT_LOGIN')
                    ]);

            return;
        }

        $hash = md5(rand(0, 1000000));
        $stmt2 = $this->db->prepare('UPDATE `'.LfConfig::get('DB_TABLE_USER').'`
                                    SET '.LfConfig::get('FORGOTTEN_DB_COLUMN_HASH').'=:hash
                                    WHERE '.LfConfig::get('DB_COLUMN_LOGIN').'=:login
                                    ');

        $stmt2->bindValue(':login', $this->getInputValue('INPUT_LOGIN'), PDO::PARAM_STR);
        $stmt2->bindValue(':hash', $hash, PDO::PARAM_STR);

        if (!$stmt2->execute()) {
            $this->msg->setError(LfText::get('ForgottenPasswordFailed'), 'SQL error.', 'Query: '.$stmt2->queryString);
            
            return;
        }

        if ($stmt2->rowCount() != 1) {
            $this->msg->setError(LfText::get('ForgottenPasswordFailed'),
                    'Hash was not updated in database',
                    [
                        'sql' => $stmt->queryString
                    ]);

            return;
        }

        $row['link'] = $this->forgottenPasswordLink(LfConfig::get('FORGOTTEN_EMAIL_LINK'), $hash, $row[LfConfig::get('DB_COLUMN_ID')]);

        // Send email?
        if (LfConfig::get('FORGOTTEN_SEND_EMAIL_AUTOMATICALLY') === true)
            $this->email->send(LfConfig::get('FORGOTTEN_EMAIL_TEMPLATE_FILE'), $row, 'Email with instruction for password change');

        // Success response
        $this->msg->setSuccess(LfText::get('ForgottenPasswordSent'), 'Email with instruction for password change was sent', $row);
    }

    /**
     * Correctly use ? or & in link
     *
     * @param string $link
     * @param string $hash
     * @param int $dbColumnId
     * @return string
     */
    private function forgottenPasswordLink($link, $hash, $dbColumnId) {
        $firstMark = '?';
        
        // If link contains "?" then use mark "&"
        $pos = strpos($link, '?');
        
        if ($pos !== false) // Found
           $firstMark = '&';
        
        return $link.$firstMark.'hash='.$hash.'&user_id='.$dbColumnId;
    }

    /**
     * Forgotten password change
     *
     * @return array|boolean
     */
    private function forgottenPasswordChange() {
        // Find user by id
        $stmt = $this->db->prepare('SELECT 
                                        '.LfConfig::get('DB_COLUMN_ID').'
                                    FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                    WHERE '.LfConfig::get('DB_COLUMN_ID').'=:user_id 
                                        AND '.LfConfig::get('FORGOTTEN_DB_COLUMN_HASH').'=:hash
                                        AND '.LfConfig::get('FORGOTTEN_DB_COLUMN_HASH').'<>""
                                    ');

        $stmt->bindValue(':user_id', $this->post['user_id'], PDO::PARAM_INT);
        $stmt->bindValue(':hash', $this->post['hash'], PDO::PARAM_STR);

        $result = $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // todo: not work
        if (!$result) {
            $this->msg->setError(LfText::get('ChangePasswordFailed'), 'SQL error.', 'Query: '.$stmt->queryString);

            return;
        }

        if ($rows === false || count($rows) == 0) {
            $this->msg->setError(LfText::get('ChangePasswordFailed'),
                    'Found no user, wrong/old link?',
                    [
                        'DB_COLUMN_ID' => $this->post['user_id'],
                        'hash value' => $this->post['user_id'],
                        'result' => $rows
                    ]);

            return;
        }

        // Change password
        $password = $this->encodePassword($this->post['password']);

        $stmt2 = $this->db->prepare('UPDATE `'.LfConfig::get('DB_TABLE_USER').'`
                            SET '.LfConfig::get('FORGOTTEN_DB_COLUMN_HASH').'="", '.LfConfig::get('DB_COLUMN_PASSWORD').'=:password
                            WHERE '.LfConfig::get('DB_COLUMN_ID').'=:user_id AND '.LfConfig::get('FORGOTTEN_DB_COLUMN_HASH').'=:hash
                            ');

        $stmt2->bindValue(':user_id', $this->post['user_id'], PDO::PARAM_INT);
        $stmt2->bindValue(':hash', $this->post['hash'], PDO::PARAM_STR);
        $stmt2->bindValue(':password', $password, PDO::PARAM_STR);

        if (!$stmt2->execute()) {
            $this->msg->setError(LfText::get('ChangePasswordFailed'), 'SQL error.', 'Query: '.$stmt2->queryString);

            return;
        }

        if ($stmt2->rowCount() != 1) {
            $this->msg->setError(LfText::get('ChangePasswordFailed'),
                    'Password was not changed in database',
                    [
                        'sql' => $stmt2->queryString
                    ]);

            return;
        }

        // Success response
        $this->msg->setSuccess(LfText::get('ChangePasswordSuccess'), ['user_id' => $this->post['user_id']]);
    }

    /**
     * Compare passwords by correct authentization type
     *
     * @param string $inputValue - Password from formular
     * @param string $dbValue - Password from database
     * @param array $authTypeArray - Array with one enabled authentization type
     * @param array $fetchedRow - All fetched columns from database for optional use in own class authentization
     * @return boolean
     */
    private function comparePassword($inputValue, $dbValue, $authTypeArray, $fetchedRow) {
        $error = false;

        if ($authTypeArray['password_verify'] === true) {
            if (!password_verify($inputValue, $dbValue))
                $error = true;
        }
        else if ($authTypeArray['plain_text'] === true) {
            if (!LfHashEquals::compare($inputValue, $dbValue))
                $error = true;
        }
        else if ($authTypeArray['md5'] === true) {
            $hash = md5($inputValue);

            if ($hash != $dbValue)
                $error = true;
        }
        else if (is_object($authTypeArray['own_class'])) {
            if (!$authTypeArray['own_class']->comparePassword($inputValue, $dbValue, $fetchedRow))
                $error = true;
        }

        if ($error)
            return false;
        else
            return true;
    }

    /**
     * Encode passwords by correct authentization type
     *
     * @param string $password - Password to encode
     * @return string - Encoded password
     */
    private function encodePassword($password) {
        $encodedPassword = 'encodePasswordMissingType';

        if (LfConfig::get('AUTH_TYPE')['password_verify'] === true)
            $encodedPassword = password_hash($password, PASSWORD_DEFAULT);
        else if (LfConfig::get('AUTH_TYPE')['plain_text'] === true)
            $encodedPassword = $password;
        else if (LfConfig::get('AUTH_TYPE')['md5'] === true)
            $encodedPassword = md5($password);
        else if (is_object(LfConfig::get('AUTH_TYPE')['own_class']))
            $encodedPassword = LfConfig::get('AUTH_TYPE')['own_class']->encodePassword($password);

        return $encodedPassword;
    }

    /**
     * Remove password from array if not defined in LOGIN_FETCH_COLUMNS
     *
     * @param array $customColumnsArray - List of columns for fetching - LOGIN_FETCH_COLUMNS
     * @param array $row - Currently fetched array (contains always also password)
     * @return array
     */
    private function removePasswordFromArray($customColumnsArray, $row) {
        if (is_null($customColumnsArray) || !in_array(LfConfig::get('DB_COLUMN_PASSWORD'), $customColumnsArray))
            unset($row[LfConfig::get('DB_COLUMN_PASSWORD')]);

        return $row;
    }

    /**
     * Validate login form and store error messages if they exists
     */
    private function loginFormValidate() {
        $this->inputLoginValidate(LfText::get('LoginFailed'));
        $this->inputPasswordValidate(LfText::get('LoginFailed'));
    }

    /**
     * Validate register form and store error messages if they exists
     */
    private function registerFormValidate() {
        $this->inputLoginValidate(LfText::get('RegisterFailed'));
        $this->inputPasswordValidate(LfText::get('RegisterFailed'));
        $this->inputPasswordVerifyValidate(LfText::get('RegisterFailed'));
    }

    /**
     * Validate forgotten password request form and store error messages if they exists
     */
    private function forgottenPasswordRequestFormValidate() {
        $this->inputLoginValidate(LfText::get('ForgottenPasswordFailed'));
    }

    /**
     * Validate forgotten password change and store error messages if they exists
     */
    private function forgottenPasswordChangeFormValidate() {
        $this->inputPasswordValidate(LfText::get('ChangePasswordFailed'));
        $this->inputPasswordVerifyValidate(LfText::get('ChangePasswordFailed'));
    }
    
    /**
     * Validate one input: login
     *
     * @param string $errorTitle - Title for error when validating will fail
     */
    private function inputLoginValidate($errorTitle) {
        $validateLoginClass =   LfConfig::get('CLASS_LOGIN_VALIDATE');
        $inputLoginName =       LfConfig::get('INPUT_LOGIN');
        $inputLoginValue =      $this->getInputValue('INPUT_LOGIN');

        if (is_object($validateLoginClass)) {
            $result = $validateLoginClass->validate($inputLoginValue);

            if ($result !== true) {
                $this->msg->setError($errorTitle, $result, [
                    'INPUT_LOGIN' => $inputLoginName,
                    'INPUT_LOGIN value' => $inputLoginValue,
                    'validate_type' => 'own class'
                ]);
            }
        }
        else if (strlen($inputLoginValue) < 1)
            $this->msg->setError($errorTitle, 'Missing login', [
                'INPUT_LOGIN' => $inputLoginName,
                'validate_type' => 'non empty string'
            ]);
    }

    /**
     * Validate one input: password
     *
     * @param string $errorTitle - Title for error when validating will fail
     */
    private function inputPasswordValidate($errorTitle) {
        $validatePasswordClass =    LfConfig::get('CLASS_PASSWORD_VALIDATE');
        $inputPasswordName =        LfConfig::get('INPUT_PASSWORD');
        $inputPasswordValue =       $this->getInputValue('INPUT_PASSWORD');

        if (is_object($validatePasswordClass)) {
            $result = $validatePasswordClass->validate($inputPasswordValue);

            if ($result !== true) {
                $this->msg->setError($errorTitle, $result, [
                    'INPUT_PASSWORD' => $inputPasswordName,
                    'INPUT_PASSWORD value' => $inputPasswordValue,
                    'validate_type' => 'own class'
                ]);
            }
        }
        else if (strlen($inputPasswordValue) < 1)
            $this->msg->setError($errorTitle, 'Missing password', [
                'INPUT_PASSWORD' => $inputPasswordName,
                'validate_type' => 'non empty string'
            ]);
    }

    /**
     * Validate one input: password_verify
     *
     * @param string $errorTitle - Title for error when validating will fail
     */
    private function inputPasswordVerifyValidate($errorTitle) {
        $inputPasswordName =        LfConfig::get('INPUT_PASSWORD');
        $inputPasswordValue =       $this->getInputValue('INPUT_PASSWORD');
        $inputPasswordVerifyName =  LfConfig::get('INPUT_PASSWORD_VERIFY');
        $inputPasswordVerifyValue = $this->getInputValue('INPUT_PASSWORD_VERIFY');

        if ($inputPasswordValue != $inputPasswordVerifyValue)
            $this->msg->setError($errorTitle, 'Passwords does not match.', [
                'INPUT_PASSWORD value' => $inputPasswordValue,
                'INPUT_PASSWORD_VERIFY value' => $inputPasswordVerifyValue,
                'INPUT_PASSWORD' => $inputPasswordName,
                'INPUT_PASSWORD_VERIFY' => $inputPasswordVerifyName,
            ]);
    }

    /**
     * Check if data are already in database
     *
     * @param array $data
     * @return boolean|array - array with duplicities
     */
    private function registerCheckUniqueUserData($data) {
        $resultArr = [];

        foreach ($data as $key => $value) {
            $this->post[$value] = trim($this->post[$value]);

            // Check if entered value is filled
            if ($this->post[$value] == '') {
                $this->msg->setError(LfText::get('RegisterFailed'), 'Empty value: '.$value);

                return false;
            }

            $stmt = $this->db->prepare('SELECT
                                    '.$value.'
                                FROM `'.LfConfig::get('DB_TABLE_USER').'`
                                WHERE '.$value.'=:'.$value.'
                                ');

            $stmt->bindValue(':'.$value, $this->post[$value], PDO::PARAM_STR);

            $result = $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if (isset($row[$value]) && $row[$value] != '')
                $resultArr[$value] = $row[$value];

            if (!$result) {
                $this->msg->setError(LfText::get('RegisterFailed'), 'SQL error.', 'Query: '.$stmt->queryString);

                return false;
            }
        }

        if (count($resultArr) > 0)
            return $resultArr;

        return true;
    }
    
    /**
     * Connect PDO instance
     *
     * Return PDO
     */
    private function databaseConnect() {
        // Do we have PDO instance already?
        $pdoInConfig = LfConfig::get('DB_PDO');

        if (!is_null($pdoInConfig)) {
            if ($pdoInConfig instanceof PDO)
                return $pdoInConfig;

            // We have PDO instance in config but it is not valid instance
            $this->msg->setError(LfText::get('DbConnectError'), 'PDO instance is set in config but it is not PDO instance.');

            return null;
        }

        // Connect
        try {
            if (LfConfig::get('SHOW_ERRORS'))
                $pdo = new PDO('mysql:host='.LfConfig::get('DB_SERVER').';dbname='.LfConfig::get('DB_NAME'), LfConfig::get('DB_LOGIN'), LfConfig::get('DB_PASSWORD'), [PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING]);
            else
                $pdo = new PDO('mysql:host='.LfConfig::get('DB_SERVER').';dbname='.LfConfig::get('DB_NAME'), LfConfig::get('DB_LOGIN'), LfConfig::get('DB_PASSWORD'));
        }
        catch (PDOException $e) {
            if (LfConfig::get('SHOW_ERRORS')) {

                // Replace db connection info when connection fail and print error with all database config values
                $messageOrg = $e->getMessage();

                $message = str_replace(LfConfig::get('DB_SERVER'), '*****', $messageOrg);
                $message = str_replace(strtolower(LfConfig::get('DB_NAME')), '*****', $message);
                $message = str_replace(LfConfig::get('DB_LOGIN'), '*****', $message);
                $message = str_replace(LfConfig::get('DB_PASSWORD'), '*****', $message);

                $this->msg->setError(LfText::get('DbConnectError'), $message, $messageOrg);
            }
            else
                $this->msg->setError(LfText::get('DbConnectError'), 'Set SHOW_ERRORS to true for printing errors');

            return null;
        }

        return $pdo;
    }
}