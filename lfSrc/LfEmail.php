<?php

/*
 * Simplified phpmailer usage
 *
 * You have to set $cfg, $msg to global config classes,
 * same instance for all classes.
 */
class LfEmail {
    public $msg; // Message class

    /*
     * Send email with replaced values in template.
     * EmailTo is $dataArray['email'].
     *
     * @param string $templateFile
     * @param array $dataArray - Array with values for replacing in template
     * @param string [$subject]
     * @param string [$emailFrom]
     * @param string [$emailTo]
     * @return boolean
     */
    public function send($templateFile, $dataArray, $subject = 'Empty subject', $emailFrom = null, $emailTo = null) {
        // Can we load phpmailer?
        $phpmailerFile = LfConfig::get('PHP_MAILER_FILE');
        if (!file_exists($phpmailerFile)) {
            $this->msg->setError(LfText::get('SendEmailFailed'), 'PHPMailerAutoload.php not found', 'PHP_MAILER_FILE: '.$phpmailerFile);

            return false;
        }

        // Is dataArray an array?
        if (!is_array($dataArray)) {
            $this->msg->setError(LfText::get('SendEmailFailed'), 'Variable dataArray is not array', $dataArray);

            return false;
        }

        // Get correct emailFrom
        if (is_null($emailFrom))
            $emailFrom = LfConfig::get('PHP_MAILER_EMAIL_FROM');

        // Validate email from
        if (!filter_var($emailFrom, FILTER_VALIDATE_EMAIL)) {
            $this->msg->setError(LfText::get('SendEmailFailed'), 'Email FROM is not valid email', $dataArray);

            return false;
        }

        // Get correct emailTo
        if (is_null($emailTo)) {
            $dbColumnEmailName = LfConfig::get('DB_COLUMN_EMAIL');
            if (isset($dataArray[$dbColumnEmailName]))
                $emailTo = $dataArray[$dbColumnEmailName];
            else {
                $this->msg->setError(LfText::get('SendEmailFailed'), 'Email TO is wrong', [
                    'note' => 'Email TO is null and therefore should be used from database automatically. Database column for email is "'.$dbColumnEmailName.'" (REGISTER_EMAIL_TO_DB_COLUMN) but key with this value not exists in $dataArray.',
                    'dataArray' => $dataArray
                ]);

                return false;
            }
        }

        // Validate email to
        if (!filter_var($emailTo, FILTER_VALIDATE_EMAIL)) {
            $this->msg->setError(LfText::get('SendEmailFailed'), 'Email TO is not valid email', $dataArray);

            return false;
        }

        // Get correct subject
        if (is_null($subject))
            $subject = LfConfig::get('PHP_MAILER_SUBJECT');

        // Check template file
        if (!file_exists($templateFile)) {
            $this->msg->setError(LfText::get('SendEmailFailed'), 'Template file not found', 'Template file: '.$templateFile);

            return false;
        }

        // Load template
        $template = file_get_contents($templateFile);

        // Replace tags __key__
        $fixedArray = [];
        foreach ($dataArray as $key => $value) {
            $fixedArray['__'.$key.'__'] = $value;
        }

        $body = strtr($template, $fixedArray);

        // Phpmailer
        require_once LfConfig::get('PHP_MAILER_FILE');

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = LfConfig::get('PHP_MAILER_SMTP');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = LfConfig::get('PHP_MAILER_USERNAME');                 // SMTP username
        $mail->Password = LfConfig::get('PHP_MAILER_PASSWORD');                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = LfConfig::get('PHP_MAILER_PORT');                                    // TCP port to connect to

        $mail->setFrom($emailFrom);
        $mail->addAddress($emailTo);     // Add a recipient
        //$mail->addReplyTo($emailTo);
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = strip_tags($body);

        // If we unit testing then all emails are successfully fake sent
        if (LfConfig::get('UNIT_TESTS_MODE') == true)
            return true;

        if (!$mail->send()) {
            $this->msg->setError(LfText::get('SendEmailFailed'), LfText::get('SendEmailFailed'), print_r($mail->ErrorInfo, true));

            return false;
        }
        else
            return true;
    }
}