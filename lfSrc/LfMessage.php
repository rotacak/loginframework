<?php

/*
 * Global error and success message handler
 *
 * You have to set $cfg to global config class, same instance for all classes.
 */
class LfMessage {
    private $errorStore = [];   // Error message
    private $successStore = []; // Success message

    /*
     * Store error message
     *
     * @param string $errorName
     * @param string $message
     * @param string [$devMessage] - Only for development, it can releval database columns etc.
     */
    public function setError($errorName, $message, $devMessage = '') {
        // Store only first error
        if (isset($this->errorStore['error']))
            return;

        $response['title'] = $errorName;
        $response['message'] = $message;

        // Development errors store only if is it enabled
        if (LfConfig::get('SHOW_ERRORS'))
            $response['dev_message'] = $devMessage;

        $this->errorStore['error'] = $response;
    }

    /*
     * Store success message
     *
     * @param string $success
     * @param string $message
     * @param string [$devMessage] - Only for development, it can releval database columns etc.
     */
    public function setSuccess($success, $message, $devMessage = '') {
        $response['title'] = $success;
        $response['message'] = $message;

        // Development errors store only if is it enabled
        if (LfConfig::get('SHOW_ERRORS'))
            $response['dev_message'] = $devMessage;

        $this->successStore['success'] = $response;
    }

    /**
     * Return error message
     *
     * @return array
     */
    public function getError() {
        return $this->errorStore;
    }

    /**
    * Return success message
    *
    * @return array
    */
    public function getSuccess() {
        return $this->successStore;
    }

    /**
     * Return true if there is atleast one error message
     *
     * @return boolean
     */
    public function isError() {
        if (count($this->errorStore) > 0)
            return true;
        else
            return false;
    }
}