<?php

/*
 * Hash_equals() with compatibility for PHP < 5.6.0
 */
class LfHashEquals {
    /*
     * Compare two strings with same time regardles if they are identical or no.
     *
     * @return boolean
     */
    public static function compare($str1, $str2) {
        if(strlen($str1) != strlen($str2)) {
          return false;
        } else {
          $res = $str1 ^ $str2;
          $ret = 0;

          for($i = strlen($res) - 1; $i >= 0; $i--) {
              $ret |= ord($res[$i]);
          }

          return !$ret;
        }
    }
}