<?php

/**
 * Global config class
 */
class LfConfig {
    private static $config;
    private static $initialized;

    private static function initialize() {
        if (self::$initialized == true)
            return;

        // Default config
        self::$config = [
            'SHOW_ERRORS' =>              false,
            'UNIT_TESTS_MODE' =>          false,

            'LOGIN_FETCH_COLUMNS' =>      ['id', 'email'],

            'REGISTER_SEND_EMAIL_AUTOMATICALLY' => false,
            'REGISTER_EMAIL_TEMPLATE_FILE' => 'lfSrc/templates/emailRegistrationTpl.html',
            'REGISTER_FETCH_COLUMNS' =>   ['id', 'email', 'password'],
            'REGISTER_SAVE_INPUTS' =>     null,

            'FORGOTTEN_SEND_EMAIL_AUTOMATICALLY' => false,
            'FORGOTTEN_EMAIL_TEMPLATE_FILE' => 'lfSrc/templates/emailForgottenPasswordTpl.html',
            'FORGOTTEN_EMAIL_LINK' =>     'http://localhost/loginframework/example.php',
            'FORGOTTEN_DB_COLUMN_HASH' => 'confirmationHash',
            'FORGOTTEN_FETCH_COLUMNS' =>  ['id'],

            '_POST' =>                    '',
            '_GET' =>                     '',
            'INPUT_LOGIN' =>              'login',
            'INPUT_PASSWORD' =>           'password',
            'INPUT_PASSWORD_VERIFY' =>    'password_verify',
            'INPUT_TYPE' =>               'type',
            'CLASS_LOGIN_VALIDATE' =>     null,
            'CLASS_PASSWORD_VALIDATE' =>  null,

            'AUTH_TYPE' => [
                                'password_verify' => false,
                                'plain_text'      => false,
                                'md5'             => true,
                                'own_class'       => false
                           ],

            'DB_PDO' =>                   '',
            'DB_SERVER' =>                '',
            'DB_LOGIN' =>                 '',
            'DB_PASSWORD' =>              '',
            'DB_NAME' =>                  '',

            'DB_TABLE_USER' =>            'user',
            'DB_COLUMN_LOGIN' =>          'email',
            'DB_COLUMN_PASSWORD' =>       'password',
            'DB_COLUMN_ID' =>             'id',
            'DB_COLUMN_EMAIL' =>          'email',

            'PHP_MAILER_FILE' =>          'vendor/phpmailer/phpmailer/PHPMailerAutoload.php',
            'PHP_MAILER_SMTP' =>          '',
            'PHP_MAILER_PORT' =>          25,
            'PHP_MAILER_USERNAME' =>      '',
            'PHP_MAILER_PASSWORD' =>      '',
            'PHP_MAILER_EMAIL_FROM' =>    ''
        ];

        self::$initialized = true;
    }

    /**
     * Set config key and value
     *
     * @param string $key
     * @param string
     */
    public static function set($key, $value) {
        self::initialize();
        self::$config[$key] = $value;
    }

    /**
     * Set complete config as array TODO: merge array into default settings
     *
     * @param array $configArray
     */
    public static function setArray($configArray) {
        self::initialize();
        self::$config = array_merge(self::$config, $configArray);
    }

    /**
     * Get config value by key
     *
     * @param string $key
     * @return string
     */
    public static function get($key) {
        self::initialize();

        // Null is also correct value
        if (array_key_exists($key, self::$config))
            return self::$config[$key];
        else
            return 'configValueForThisKeyNotSet:'.$key;
    }
}