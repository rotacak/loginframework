<?php

/**
 * All texts
 */
class LfText {
    private static $txt = [];
    private static $initialized;

    private static function initialize() {
        if (self::$initialized == true)
            return;

        self::$txt = [
            'LoginFailed' =>                     'Login failed',
            'LoginSuccess' =>                    'Login success',
            'RegisterFailed' =>                  'Registration failed',
            'RegisterSuccessButSomeErrors' =>    'Registration success but some errors appears',
            'RegisterSuccess' =>                 'Registration success',
            'DbConnectError' =>                  'Database connect error',
            'SendEmailFailed' =>                 'Send email failed',
            'ForgottenPasswordFailed' =>         'Forgotten password - error',
            'ForgottenPasswordSent' =>           'Forgotten password - email sent',
            'ChangePasswordFailed' =>            'Change password failed',
            'ChangePasswordSuccess' =>           'Change password success',
            'WrongInputType' =>                  'Input "type" is wrong or missing'
        ];

        self::$initialized = true;
    }

    /**
     * Get text value by key
     *
     * @param string $key
     * @return string
     */
    public static function get($key) {
        self::initialize();

        if (array_key_exists($key, self::$txt))
            return self::$txt[$key];
        else
            return 'textValueForThisKeyNotSet:'.$key;
    }
}