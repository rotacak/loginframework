<?php
/*
    You have to move this example file into root
 */

// Custom password conparing class
class CustomPasswordCompare {
    /*
     * Compare passwords by own algorithm
     *
     * @param string $inputValue - Password from formular
     * @param string $dbValue - Password from database
     * @param array $fetchedRow - All fetched columns from database for optional use
     * @return boolean
     */
    public function comparePassword($inputValue, $dbValue, $fetchedRow) {
        // Old md5 authentization
        if (strlen($dbValue) <= 32)
            return md5($inputValue) == $dbValue;

        // New comparsion
        include 'PasswordHash.php'; // Download this from http://www.openwall.com/phpass/ and copy it into examples folder

        $hasher = new PasswordHash(8, true);

        return $hasher->CheckPassword($inputValue, $dbValue);
    }
}

// If form is sent
if (isset($_POST['type'])) {
    include 'lfSrc/LoginFrameworkAutoload.php';

    $config = [
        // Framework config
        'SHOW_ERRORS' =>              true,                                                 // Show all development errors {bool}
        'UNIT_TESTS_MODE' =>          false,                                                // Not sending any emails in unit tests {bool}

        // Login config
        'LOGIN_FETCH_COLUMNS' =>      ['id', 'user_email', 'user_registered'],              // Column values returned in login success response {array}

        // HTML form config
        '_POST' =>                    $_POST,                                               // POST data {array}
        'INPUT_LOGIN' =>              'login',                                              // Input name for login {string}
        'INPUT_PASSWORD' =>           'password',                                           // Input name for password {string}
        'INPUT_TYPE' =>               'type',                                               // Input name for type {string} Values: login, register, forgottenPasswordRequest, forgottenPasswordChange
        'CLASS_LOGIN_VALIDATE' =>     null,                                                 // Custom login validate class {class|null}
        'CLASS_PASSWORD_VALIDATE' =>  null,                                                 // Custom password validate class {class|null}

        // Password authentization config
        'AUTH_TYPE' => [                                                                    // What authentization type will be used? {array}
                            'password_verify' => false,                                     // {bool}
                            'plain_text'      => false,                                     // {bool}
                            'md5'             => false,                                     // {bool}
                            'own_class'       => new CustomPasswordCompare()                // Custom class or false {bool|class} Ex.: new SomeClass()
                       ],

        // Mysql connection config
        'DB_PDO' =>                   null,                                                 // PDO object if you are already connected to db {PDO|null}
        'DB_SERVER' =>                '127.0.0.1',                                          // Database server address {string}
        'DB_LOGIN' =>                 'root',                                               // Database username {string}
        'DB_PASSWORD' =>              '',                                                   // Database password {string}
        'DB_NAME' =>                  'your_db',                                            // Database name {string}

        // Mysql structure config
        'DB_TABLE_USER' =>            'wp_users',                                           // What table is considered as user table? {string}
        'DB_COLUMN_LOGIN' =>          'user_nicename',                                      // What column in user table is considered as login? {string}
        'DB_COLUMN_PASSWORD' =>       'user_pass',                                          // What column in user table is considered as password? {string}
        'DB_COLUMN_ID' =>             'id',                                                 // What column in user table is considered as user id? {string}
    ];

    $lf = new LoginFramework($config);

    // Run framework
    $lfResponse = $lf->run();

    // Capture response
    if (isset($lfResponse['error']) || isset($lfResponse['success']))
        echo '<pre>'.print_r($lfResponse, true).'</pre>';
}
?>

<fieldset>
    <legend>Login form</legend>
    <form method="post">
        Login: <input type="text" name="login"><br>
        Password: <input type="password" name="password"><br>
        <input type="hidden" name="type" value="login">
        <input type="submit" value="Send">
    </form>
</fieldset>