<?php
// Example custom input validation class
class LoginValidate {
    /**
     * Validate value by own algorithm
     *
     * @param string $inputValue - Login from formular
     * @return boolean
     */
    public function validate($inputValue) {
        if ($inputValue == 'x')
            return 'Login "x" is not allowed.'; // Error
        else
            return true;
    }
}

// Example custom password conparing class
class CustomPasswordCompare {
    /**
     * Compare passwords by own algorithm
     *
     * @param string $inputValue - Password from formular
     * @param string $dbValue - Password from database
     * @param array $fetchedRow - All fetched columns from database for optional use
     * @return boolean
     */
    public function comparePassword($inputValue, $dbValue, $fetchedRow) {
        // Only "aa" is correct password format in this case
        if ($inputValue.$dbValue == 'aa')
            return true;
        else
            return false;
    }
}

// If form is sent
include 'lfSrc/loginFrameworkAutoload.php';

$config = [
        // Framework config
        'SHOW_ERRORS' =>              true,                                                 // Show all development errors {bool}
        'UNIT_TESTS_MODE' =>          false,                                                // Not sending any emails in unit tests {bool}

        // Login config
        'LOGIN_FETCH_COLUMNS' =>      ['id', 'email'],                                      // Column values returned in login success response {array|null}

        // Register config
        'REGISTER_SEND_EMAIL_AUTOMATICALLY' => false,                                       // Send "you are registered" email automatically after form is sent {bool}
        'REGISTER_EMAIL_TEMPLATE_FILE' => 'lfSrc/templates/emailRegistrationTpl.html',      // Path to registration template {string}
        'REGISTER_FETCH_COLUMNS' =>   ['id', 'email', 'password'],                          // Column values returned in success response {array|null}
        'REGISTER_SAVE_INPUTS' =>     ['nick' => 'unique', 'color' => 'any'],               // Column values also saved into database with same name, can do unique check {array|null} Ex.: ['nick' => 'unique', 'color' => 'any']

        // Forgotten passsword config
        'FORGOTTEN_SEND_EMAIL_AUTOMATICALLY' => true,                                       // Send "change forgotten password" email automatically after form is sent {bool}
        'FORGOTTEN_EMAIL_TEMPLATE_FILE' => 'lfSrc/templates/emailForgottenPasswordTpl.html',// Path to forgottend password template {string}
        'FORGOTTEN_EMAIL_LINK' =>     'http://localhost/loginframework/example.php',        // Link to page where you will find form for forgotten password change {string}
        'FORGOTTEN_DB_COLUMN_HASH' => 'confirmationHash',                                   // What column in user table will be used for storing hash for forgotten password (varchar 200)? {string}
        'FORGOTTEN_FETCH_COLUMNS' =>  ['id'],                                               // Column values returned in success response {array|null}

        // HTML form config
        '_POST' =>                    $_POST,                                               // POST data {array}
        '_GET' =>                     $_GET,                                                // GET data {array}
        'INPUT_LOGIN' =>              'login',                                              // Input name for login {string}
        'INPUT_PASSWORD' =>           'password',                                           // Input name for password {string}
        'INPUT_PASSWORD_VERIFY' =>    'password_verify',                                    // Input name for password verify {string}
        'INPUT_TYPE' =>               'type',                                               // Input name for type {string} Values: login, register, forgottenPasswordRequest, forgottenPasswordChange
        'CLASS_LOGIN_VALIDATE' =>     new LoginValidate(),                                  // Custom login validate class {class|null}
        'CLASS_PASSWORD_VALIDATE' =>  null,                                                 // Custom password validate class {class|null}

        // Password authentization config
        'AUTH_TYPE' => [                                                                    // What authentization type will be used? {array}
                            'password_verify' => true,                                      // {bool}
                            'plain_text'      => false,                                     // {bool}
                            'md5'             => false,                                     // {bool}
                            'own_class'       => false                                      // Custom class or false {bool|class} Ex.: new SomeClass()
                       ],

        // Mysql connection config
        'DB_PDO' =>                   null,                                                 // PDO object if you are already connected to db {PDO|null}
        'DB_SERVER' =>                '127.0.0.1',                                          // Database server address {string}
        'DB_LOGIN' =>                 'root',                                               // Database username {string}
        'DB_PASSWORD' =>              '',                                                   // Database password {string}
        'DB_NAME' =>                  'loginframework',                                     // Database name {string}

        // Mysql structure config
        'DB_TABLE_USER' =>            'user',                                               // What table is considered as user table? {string}
        'DB_COLUMN_LOGIN' =>          'email',                                              // What column in user table is considered as login? {string}
        'DB_COLUMN_PASSWORD' =>       'password',                                           // What column in user table is considered as password? {string}
        'DB_COLUMN_ID' =>             'id',                                                 // What column in user table is considered as user id? {string}
        'DB_COLUMN_EMAIL' =>          'email',                                              // What column in user table is considered as email? {string}

        // Emailing config
        'PHP_MAILER_FILE' =>          'vendor/phpmailer/phpmailer/PHPMailerAutoload.php',   // Path to phpmailer file PHPMailerAutoload.php {string}
        'PHP_MAILER_SMTP' =>          'mailtrap.io',                                        // SMTP server address {string}
        'PHP_MAILER_PORT' =>          25,                                                   // SMTP server port {int}
        'PHP_MAILER_USERNAME' =>      '551296d14c7b38ddc',                                  // SMTP username {string}
        'PHP_MAILER_PASSWORD' =>      '3d0a26a47c6430',                                     // SMTP password {string}
        'PHP_MAILER_EMAIL_FROM' =>    'lf@loginframework.cz'                                // Email displayed in FROM field {string}
    ];

$lf = new LoginFramework($config);

// Run framework
$lfResponse = $lf->run();

// Capture response
if (isset($lfResponse['error']) || isset($lfResponse['success']))
    echo '<pre>'.print_r($lfResponse, true).'</pre>';

// Possible to use
// $dbConnection = $lf->getPDO();
//
// Send registration email
// $lf->email->send($dataArray);
?>

<fieldset>
    <legend>Login form</legend>
    <form method="post">
        Login: <input type="text" name="login"><br>
        Password: <input type="password" name="password"><br>
        <input type="hidden" name="type" value="login">
        <input type="submit" value="Send">
    </form>
</fieldset>

<fieldset>
    <legend>Register form</legend>
    <form method="post">
        Login: <input type="text" name="login"><br>
        Password: <input type="password" name="password"><br>
        Password verify: <input type="password" name="password_verify"><br>
        <input type="hidden" name="type" value="register">
        <input type="submit" value="Send">
    </form>
</fieldset>

<fieldset>
    <legend>Forgotten password request form</legend>
    <form method="post">
        Login: <input type="text" name="login"><br>
        <input type="hidden" name="type" value="forgottenPasswordRequest">
        <input type="submit" value="Send">
    </form>
</fieldset>

<fieldset>
    <legend>Forgotten password change form</legend>
    <form method="post">
        New password: <input type="password" name="password"><br>
        New password verify: <input type="password" name="password_verify"><br>
        <input type="hidden" name="type" value="forgottenPasswordChange">
        <input type="hidden" name="hash" value="<?php echo isset($_REQUEST['hash']) ? $_REQUEST['hash'] : ''; ?>">
        <input type="hidden" name="user_id" value="<?php echo isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : ''; ?>">
        <input type="submit" value="Send">
    </form>
</fieldset>