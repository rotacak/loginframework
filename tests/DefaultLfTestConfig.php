<?php

class DefaultLfTestConfig {
    public static function getArray() {
        return [
            'SHOW_ERRORS' =>              true,
            'UNIT_TESTS_MODE' =>          true,

            'LOGIN_FETCH_COLUMNS' =>      ['id', 'email'],

            'REGISTER_SEND_EMAIL_AUTOMATICALLY' => false,
            'REGISTER_EMAIL_TEMPLATE_FILE' => 'lfSrc/templates/emailRegistrationTpl.html',
            'REGISTER_FETCH_COLUMNS' =>   ['id', 'email', 'password'],
            'REGISTER_SAVE_INPUTS' =>     null,

            'FORGOTTEN_SEND_EMAIL_AUTOMATICALLY' => false,
            'FORGOTTEN_EMAIL_TEMPLATE_FILE' => '',
            'FORGOTTEN_EMAIL_LINK' =>     '',
            'FORGOTTEN_DB_COLUMN_HASH' => 'confirmationHash',
            'FORGOTTEN_FETCH_COLUMNS' =>  ['id'],

            '_POST' =>                    '',
            '_GET' =>                     '',
            'INPUT_LOGIN' =>              'login',
            'INPUT_PASSWORD' =>           'password',
            'INPUT_PASSWORD_VERIFY' =>    'password_verify',
            'INPUT_TYPE' =>               'type',
            'CLASS_LOGIN_VALIDATE' =>     null,
            'CLASS_PASSWORD_VALIDATE' =>  null,

            'AUTH_TYPE' => [
                                'password_verify' => true,
                                'plain_text'      => false,
                                'md5'             => false,
                                'own_class'       => false
                           ],

            'DB_PDO' =>                   '',
            'DB_SERVER' =>                '',
            'DB_LOGIN' =>                 '',
            'DB_PASSWORD' =>              '',
            'DB_NAME' =>                  '',

            'DB_TABLE_USER' =>            'user',
            'DB_COLUMN_LOGIN' =>          'email',
            'DB_COLUMN_PASSWORD' =>       'password',
            'DB_COLUMN_ID' =>             'id',
            'DB_COLUMN_EMAIL' =>          'email',

            'PHP_MAILER_FILE' =>          'vendor/phpmailer/phpmailer/PHPMailerAutoload.php',
            'PHP_MAILER_SMTP' =>          '',
            'PHP_MAILER_PORT' =>          25,
            'PHP_MAILER_USERNAME' =>      '',
            'PHP_MAILER_PASSWORD' =>      '',
            'PHP_MAILER_EMAIL_FROM' =>    ''
        ];
    }
}