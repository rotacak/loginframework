<?php
class LfEmailTest extends PHPUnit_Framework_TestCase {
    protected function setUp() {
        parent::setUp();

        $this->email = new LfEmail();
        $this->email->msg = new LfMessage();

        LfConfig::setArray(DefaultLfTestConfig::getArray());       
    }

    public function testEmailSendDirectOk() {
        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['login' => 'John'], 'Subject', 'from@loginframework.cz', 'to@loginframework.cz');

        $this->assertTrue($result);
    }

    public function testEmailSendNotDirectOk() {
        LfConfig::set('PHP_MAILER_EMAIL_FROM', 'from@loginframework.cz');
        LfConfig::set('DB_COLUMN_EMAIL', 'emailTo');

        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['emailTo' => 'to@loginframework.cz']);

        $this->assertTrue($result);
    }

    public function testPhpmailerFileFail() {
        LfConfig::set('PHP_MAILER_FILE', 'nonsense');

        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['login' => 'John'], 'Subject', 'from@loginframework.cz', 'to@loginframework.cz');

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('PHPMailerAutoload.php not found', $this->email->msg->getError()['error']['message']);
    }

    public function testNotArray() {
        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', 'notArray', 'Subject', 'from@loginframework.cz', 'to@loginframework.cz');

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Variable dataArray is not array', $this->email->msg->getError()['error']['message']);
    }

    public function testEmailNotInArray() {
        LfConfig::set('PHP_MAILER_EMAIL_FROM', 'from@loginframework.cz');
        LfConfig::set('DB_COLUMN_EMAIL', 'emailTo');

        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['emailToWrong' => '']);

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Email TO is wrong', $this->email->msg->getError()['error']['message']);
    }

    public function testEmailFromInvalidDirect() {
        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['login' => 'John'], 'Subject', 'from-loginframework.cz', 'to@loginframework.cz');

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Email FROM is not valid email', $this->email->msg->getError()['error']['message']);
    }

    public function testEmailFromInvalidNotDirect() {
        LfConfig::set('PHP_MAILER_EMAIL_FROM', 'from-loginframework.cz');

        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['login' => 'John']);

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Email FROM is not valid email', $this->email->msg->getError()['error']['message']);
    }

    public function testEmailToInvalidDirect() {
        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['login' => 'John'], 'Subject', 'from@loginframework.cz', 'to-loginframework.cz');

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Email TO is not valid email', $this->email->msg->getError()['error']['message']);
    }

    public function testEmailToInvalidNotDirect() {
        LfConfig::set('PHP_MAILER_EMAIL_FROM', 'from@loginframework.cz');
        LfConfig::set('DB_COLUMN_EMAIL', 'emailTo');

        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.html', ['emailTo' => 'to-loginframework.cz']);

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Email TO is not valid email', $this->email->msg->getError()['error']['message']);
    }

    public function testTemplateLoadFailed() {
        $result = $this->email->send('lfSrc/templates/emailRegistrationTpl.XYZ', ['login' => 'John'], 'Subject', 'from@loginframework.cz', 'to@loginframework.cz');

        $this->assertFalse($result);
        $this->assertEquals('Send email failed', $this->email->msg->getError()['error']['title']);
        $this->assertEquals('Template file not found', $this->email->msg->getError()['error']['message']);
    }
}