<?php
class LfConfigTest extends PHPUnit_Framework_TestCase {
    protected function setUp() {
        parent::setUp();

        LfConfig::setArray(DefaultLfTestConfig::getArray());
    }

    public function testSetGetCfg() {
        $this->assertEquals('password', LfConfig::get('INPUT_PASSWORD'));

        LfConfig::set('INPUT_PASSWORD', 'password new');

        $this->assertEquals('password new', LfConfig::get('INPUT_PASSWORD'));
    }

    public function testSetGetCfgNull() {
        LfConfig::set('INPUT_PASSWORD', null);
        $this->assertNull(LfConfig::get('INPUT_PASSWORD'));
    }

    public function testGetCfgNotSet() {
        $this->assertEquals('configValueForThisKeyNotSet:NOTSET', LfConfig::get('NOTSET'));
    }

    public function testSetArrayMerge() {
        $this->assertEquals('configValueForThisKeyNotSet:new_key', LfConfig::get('new_key'));
        $this->assertEquals('password', LfConfig::get('INPUT_PASSWORD'));

        LfConfig::setArray(
                    [
                        'new_key' => 'new_value',
                        'INPUT_PASSWORD' => 'password_new_value'
                    ]
                );

        $this->assertEquals('new_value', LfConfig::get('new_key'));
        $this->assertEquals('password_new_value', LfConfig::get('INPUT_PASSWORD'));
    }
}
