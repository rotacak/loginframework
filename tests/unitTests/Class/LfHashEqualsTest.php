<?php
class LfHashEqualsTest extends PHPUnit_Framework_TestCase {
    public function testCompareIdentical() {
        $this->assertTrue(LfHashEquals::compare('abcd123', 'abcd123'));
    }

    public function testCompareNotIdentical() {
        $this->assertFalse(LfHashEquals::compare('abcd123', 'mnbvc'));
    }
}
