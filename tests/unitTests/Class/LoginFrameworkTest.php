<?php
// Example custom input validation class
class CustomLoginValidate {
    /**
     * Validate value by own algorithm
     *
     * @param string $inputValue - Login from formular
     * @return boolean
     */
    public function validate($inputValue) {
        if ($inputValue == 'x')
            return 'Login "x" is not allowed.'; // Error
        else
            return true;
    }
}

// Example custom password validating class
class CustomPasswordValidate {
    /**
     * Validate value by own algorithm
     *
     * @param string $inputValue - Password from formular
     * @return boolean
     */
    public function validate($inputValue) {
        if ($inputValue == 'x')
            return 'Password "x" is not allowed.'; // Error
        else
            return true;
    }
}

// Example custom password conparing class
class CustomPasswordCompare {
    /*
     * Compare passwords by own algorithm
     *
     * @param string $inputValue - Password from formular
     * @param string $dbValue - Password from database
     * @param array $fetchedRow - All fetched columns from database for optional use
     * @return boolean
     */
    public function comparePassword($inputValue, $dbValue, $fetchedRow) {
        // We know that password in tests will be "a"
        if ($inputValue.$dbValue.$fetchedRow['id'] == 'aa2')
            return true;
        else
            return false;
    }

    /**
     * Encode value by own algorithm
     *
     * @param string $password - Password from formular
     * @return string
     */
    public function encodePassword($password) {
        return 'xx'.$password.'xx';
    }
}

class LoginFrameworkTest extends DBTestCase {
    private $lf;
    private $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'a', // encoded: $2y$10$06nSRGC5Z9FikKPupPlqteCk/NOLPsbz5owOy.sWHn9XmvyBMbzTO
                    'type' => 'login'
                ];
    private $loginDataLive = [
                    'login' => 'live@loginframework.cz',
                    'password' => 'b', // encoded: $2y$10$Ld1jnjs.FmlwUCmMbs3ry.3buq8RbbZINaEuC53uJrPJ9t/gxV9B2
                    'type' => 'login'
                ];
    private $registerDataOk = [
                    'login' => 'register@loginframework.cz',
                    'password' => 'a',
                    'password_verify' => 'a',
                    'type' => 'register',
                    'nick' => 'myNick',
                    'color' => 'myColor'
                ];
    private $registerDataOkEmptyNick = [
                    'login' => 'register@loginframework.cz',
                    'password' => 'a',
                    'password_verify' => 'a',
                    'type' => 'register',
                    'nick' => '',
                    'color' => 'myColor'
                ];
    private $registerDataCustomDataExist = [
                    'login' => 'register@loginframework.cz',
                    'password' => 'a',
                    'password_verify' => 'a',
                    'type' => 'register',
                    'nick' => 'nickexist1',
                    'color' => 'colorexist3'
                ];
    private $forgottenPasswordRequestData = [
                    'login' => 'test@loginframework.cz',
                    'type' => 'forgottenPasswordRequest'
                ];
    private $forgottenPasswordChangeData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'password_new',
                    'password_verify' => 'password_new',
                    'user_id' => 1,
                    'hash' => '12345',
                    'type' => 'forgottenPasswordChange'
                ];

    protected function setUp() {
        parent::setUp();

        $config = DefaultLfTestConfig::getArray();
        $this->lf = new LoginFramework($config);

        LfConfig::set('_POST',                $this->loginData);
        LfConfig::set('DB_PDO',               $this->pdo);
    }
   
    public function testGetInputValue() {
        $this->lf->run();
        $this->assertEquals('a', $this->lf->getInputValue('INPUT_PASSWORD'));
    }

    // Use db: loginframework
    public function testMysqlDefinedConnection() {
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1');
        LfConfig::set('DB_LOGIN',             'root');
        LfConfig::set('DB_PASSWORD',          '');
        LfConfig::set('DB_NAME',              'loginframework');
        LfConfig::set('_POST',                $this->loginDataLive);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('live@loginframework.cz', $result['success']['message']['email']);
    }

    // Use db: loginframework (password is not printed at all - cannot be tested)
    public function testMysqlDefinedConnectionFailStrippedCredentialsLogin() {
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1');
        LfConfig::set('DB_LOGIN',             'rootNONSENSE');
        LfConfig::set('DB_PASSWORD',          'passwordNONSENSE');
        LfConfig::set('DB_NAME',              'loginframeworkNONSENSE');

        $result = $this->lf->run();
        $this->assertEquals('Database connect error', $result['error']['title']);
        $this->assertEquals('SQLSTATE[HY000] [1045] Access denied for user \'*****\'@\'localhost\' (using password: YES)', $result['error']['message']);
    }

    // Use db: loginframework
    public function testMysqlDefinedConnectionFailStrippedCredentialsDatabase() {
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1');
        LfConfig::set('DB_LOGIN',             'root');
        LfConfig::set('DB_PASSWORD',          '');
        LfConfig::set('DB_NAME',              'loginframeworkNONSENSE');

        $result = $this->lf->run();
        $this->assertEquals('Database connect error', $result['error']['title']);
        $this->assertEquals('SQLSTATE[HY000] [1049] Unknown database \'*****\'', $result['error']['message']);
    }

    // Use db: loginframework
    public function testMysqlDefinedConnectionFailErrorsDisabled() {
        LfConfig::set('SHOW_ERRORS',          false);
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1');
        LfConfig::set('DB_LOGIN',             'rootNONSENSE');
        LfConfig::set('DB_PASSWORD',          'passwordNONSENSE');
        LfConfig::set('DB_NAME',              'loginframeworkNONSENSE');

        $result = $this->lf->run();
        $this->assertEquals('Database connect error', $result['error']['title']);
        $this->assertEquals('Set SHOW_ERRORS to true for printing errors', $result['error']['message']);
    }

    public function testNotPDOObject() {
        LfConfig::set('DB_PDO',               []);

        $result = $this->lf->run();
        $this->assertEquals('Database connect error', $result['error']['title']);
        $this->assertEquals('PDO instance is set in config but it is not PDO instance.', $result['error']['message']);
    }

    // Use db: loginframework_tests
    public function testMysqlPHPUnitConnection() {
        LfConfig::set('LOGIN_FETCH_COLUMNS',     ['email']);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result['success']['message']['email']);
    }

    public function testWrongInputType() {
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'a',
                    'type' => 'wrongOne'
                ];

        LfConfig::set('_POST',                  $loginData);

        $result = $this->lf->run();
        $this->assertEquals('Input "type" is wrong or missing', $result['error']['title']);
        $this->assertEquals('Wrong input type', $result['error']['message']);
    }

    public function testLoginValidateEmpty() {
        $loginData = [
                    'login' => '',
                    'password' => 'a',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);

        $result = $this->lf->run();
        $this->assertEquals('Login failed', $result['error']['title']);
        $this->assertEquals('Missing login', $result['error']['message']);
    }

    public function testLoginValidateByOwnClassSuccess() {
        LfConfig::set('CLASS_LOGIN_VALIDATE',new CustomLoginValidate());

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result['success']['message']['email']);
    }

    public function testLoginValidateByOwnClassFail() {
        $loginData = [
                    'login' => 'x',
                    'password' => 'a',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('CLASS_LOGIN_VALIDATE',new CustomLoginValidate());

        $result = $this->lf->run();
        $this->assertEquals('Login failed', $result['error']['title']);
        $this->assertEquals('Login "x" is not allowed.', $result['error']['message']);
    }

    public function testCustomFetchColumnsWithoutPassword() {
        LfConfig::set('LOGIN_FETCH_COLUMNS',     ['id', 'email']);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result['success']['message']['email']);
        $this->assertEquals(1, $result['success']['message']['id']);
        $this->assertFalse(array_key_exists(LfConfig::get('DB_COLUMN_PASSWORD'), $result['success']['message']));
    }

    public function testCustomFetchColumnsWithPassword() {
        LfConfig::set('LOGIN_FETCH_COLUMNS',     ['id', 'email', 'password']);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result['success']['message']['email']);
        $this->assertEquals(1, $result['success']['message']['id']);
        $this->assertNotEmpty($result['success']['message'][LfConfig::get('DB_COLUMN_PASSWORD')]);
    }

    public function testPasswordValidateEmpty() {
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => '',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);

        $result = $this->lf->run();
        $this->assertEquals('Login failed', $result['error']['title']);
        $this->assertEquals('Missing password', $result['error']['message']);
    }

    public function testPasswordValidateByOwnClassSuccess() {
        LfConfig::set('CLASS_PASSWORD_VALIDATE',new CustomPasswordValidate());

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result['success']['message']['email']);
    }

    public function testPasswordValidateByOwnClassFail() {
        $loginData = [
                    'login' => 'testMD5@loginframework.cz',
                    'password' => 'x',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('CLASS_PASSWORD_VALIDATE',new CustomPasswordValidate());

        $result = $this->lf->run();
        $this->assertEquals('Login failed', $result['error']['title']);
        $this->assertEquals('Password "x" is not allowed.', $result['error']['message']);
    }

    public function testPasswordAuthPlainText() {
        $loginData = [
                    'login' => 'testPlainText@loginframework.cz',
                    'password' => 'a',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => true,
                            'md5'               => false,
                            'own_class'         => false
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('testPlainText@loginframework.cz', $result['success']['message']['email']);
    }

    public function testPasswordAuthMD5() {
        $loginData = [
                    'login' => 'testMD5@loginframework.cz',
                    'password' => 'a', // encoded: 0cc175b9c0f1b6a831c399e269772661
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => true,
                            'own_class'         => false
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('testMD5@loginframework.cz', $result['success']['message']['email']);
    }

    public function testPasswordAuthOwnClassWithCombinationWithFetchedColumns() {
        $loginData = [
                    'login' => 'testPlainText@loginframework.cz',
                    'password' => 'a', // encoded: 0cc175b9c0f1b6a831c399e269772661
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => false,
                            'own_class'         => new CustomPasswordCompare()
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Login success', $result['success']['title']);
        $this->assertEquals('testPlainText@loginframework.cz', $result['success']['message']['email']);
    }

    public function testPasswordAuthOwnClassFail() {
        $loginData = [
                    'login' => 'testPlainText@loginframework.cz',
                    'password' => 'x',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => false,
                            'own_class'         => new CustomPasswordCompare()
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Login failed', $result['error']['title']);
        $this->assertEquals('Wrong password', $result['error']['message']);
    }

    public function testGetPDOAfterRun() {
        $this->lf->run();
        $this->assertTrue($this->lf->getPDO() instanceof PDO);
    }

    public function testGetPDOWithoutRun() {
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1');
        LfConfig::set('DB_LOGIN',             'root');
        LfConfig::set('DB_PASSWORD',          '');
        LfConfig::set('DB_NAME',              'loginframework');

        $this->assertTrue($this->lf->getPDO() instanceof PDO);
    }

    public function testGetPDOFail() {
        LfConfig::set('DB_PDO',               null);
        LfConfig::set('DB_SERVER',            '127.0.0.1'); // Not "NONSENSE" just because of unit tests because it will wait one second and trying to resolve hostname
        LfConfig::set('DB_LOGIN',             'NONSENSE');
        LfConfig::set('DB_PASSWORD',          'NONSENSE');
        LfConfig::set('DB_NAME',              'NONSENSE');

        $result = $this->lf->getPDO();
        $this->assertEquals('Database connect error', $result['error']['title']);
    }

    public function testRegister() {
        LfConfig::set('_POST',                $this->registerDataOk);

        $result = $this->lf->run();
        $this->assertEquals('Registration success', $result['success']['title']);
        $this->assertTrue(is_array($result['success']['message']));
        $this->assertArrayHasKey('user_id', $result['success']['message']);
        $this->assertEquals(4, $result['success']['message']['user_id']);
        $this->assertEquals('register@loginframework.cz', $result['success']['message']['email']);
    }

    public function testRegisterAndSaveCustomInputs() {
        LfConfig::set('_POST',                $this->registerDataOk);
        LfConfig::set('REGISTER_SAVE_INPUTS', ['nick' => 'any', 'color' => 'unique']);

        $result = $this->lf->run();
        $this->assertEquals('Registration success', $result['success']['title']);
        $this->assertTrue(is_array($result['success']['message']));
        $this->assertEquals('myNick', $result['success']['message']['nick']);
        $this->assertEquals('myColor', $result['success']['message']['color']);
    }

    public function testRegisterAndSaveCustomInputsAlreadyInDB() {
        LfConfig::set('_POST',                $this->registerDataCustomDataExist);
        LfConfig::set('REGISTER_SAVE_INPUTS', ['nick' => 'unique', 'color' => 'unique']);

        $result = $this->lf->run();
        $this->assertEquals('Registration failed', $result['error']['title']);
        $this->assertTrue(is_array($result['error']['message']));
        $this->assertEquals('nickExist1', $result['error']['message']['nick']);
        $this->assertEquals('colorExist3', $result['error']['message']['color']);
    }

    public function testRegisterAndSaveCustomEmptyInputs() {
        LfConfig::set('_POST',                $this->registerDataOkEmptyNick);
        LfConfig::set('REGISTER_SAVE_INPUTS', ['nick' => 'unique', 'color' => 'unique']);

        $result = $this->lf->run();
        $this->assertEquals('Registration failed', $result['error']['title']);
        $this->assertEquals('Empty value: nick', $result['error']['message']);
    }

    public function testRegisterAndSendEmail() {
        LfConfig::set('_POST',                $this->registerDataOk);
        LfConfig::set('REGISTER_SEND_EMAIL_AUTOMATICALLY',  true);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => true,
                            'own_class'         => false
                         ]);

        $this->lf->email = $this->getMockBuilder('Email')
                                ->setMethods(array('send'))
                                ->getMock();

        $this->lf->email->expects($this->once())
                        ->method('send')
                        ->with(
                                $this->stringContains('lfSrc/templates/emailRegistrationTpl.html'),
                                $this->identicalTo(
                                    [
                                        'id' => '4',
                                        'email' => 'register@loginframework.cz',
                                        'password' => '0cc175b9c0f1b6a831c399e269772661'
                                    ]),
                                $this->stringContains('Registration complete')
                        );

        $result = $this->lf->run();
        $this->assertEquals('Registration success', $result['success']['title']);
        $this->assertTrue(is_array($result['success']['message']));
        $this->assertArrayHasKey('user_id', $result['success']['message']);
        $this->assertEquals(4, $result['success']['message']['user_id']);
    }

    public function testRegisterWithoutReturningUserId() {
        $registerData = [
                        'login' => 'register@loginframework.cz',
                        'password' => 'a',
                        'password_verify' => 'a',
                        'type' => 'register'
                    ];

        LfConfig::set('_POST',                $registerData);
        LfConfig::set('DB_COLUMN_ID',         null);

        $result = $this->lf->run();
        $this->assertEquals('Registration success', $result['success']['title']);
        $this->assertEquals('', $result['success']['message']['user_id']);
    }

    public function testRegisterPasswordVerifyFail() {
        $registerData = [
                        'login' => 'register@loginframework.cz',
                        'password' => 'a',
                        'password_verify' => 'b',
                        'type' => 'register'
                    ];

        LfConfig::set('_POST',                $registerData);

        $result = $this->lf->run();
        $this->assertEquals('Passwords does not match.', $result['error']['message']);
    }

    public function testRegisterUserAlreadyExists() {
        $registerData = [
                        'login' => 'test@loginframework.cz',
                        'password' => 'a',
                        'password_verify' => 'a',
                        'type' => 'register'
                    ];

        LfConfig::set('_POST',                $registerData);

        $result = $this->lf->run();
        $this->assertEquals('Login "test@loginframework.cz" is already registered', $result['error']['message']);
    }

    public function testForgottenPasswordRequestSent() {
        LfConfig::set('_POST',                $this->forgottenPasswordRequestData);

        $result = $this->lf->run();
        $this->assertEquals('Forgotten password - email sent', $result['success']['title']);
        $this->assertEquals('Email with instruction for password change was sent', $result['success']['message']);
    }

    public function testForgottenPasswordRequestLoginNotFound() {
        $forgottenPasswordRequestData = [
                    'login' => 'x',
                    'type' => 'forgottenPasswordRequest'
                ];
        LfConfig::set('_POST',                $forgottenPasswordRequestData);

        $result = $this->lf->run();
        $this->assertEquals('Forgotten password - error', $result['error']['title']);
        $this->assertEquals('Login "x" not found.', $result['error']['message']);
    }

    public function testForgottenPasswordChangeUserNotFound() {
        $forgottenPasswordChangeData = [
                            'login' => 'test@loginframework.cz',
                            'password' => 'password_new',
                            'password_verify' => 'password_new',
                            'user_id' => 123,
                            'hash' => '12345',
                            'type' => 'forgottenPasswordChange'
                        ];
        LfConfig::set('_POST',                $forgottenPasswordChangeData);

        $result = $this->lf->run();
        $this->assertEquals('Change password failed', $result['error']['title']);
        $this->assertEquals('Found no user, wrong/old link?', $result['error']['message']);
    }

    public function testForgottenPasswordChangeAuthPV() {
        LfConfig::set('_POST',                $this->forgottenPasswordChangeData);

        $result = $this->lf->run();
        $this->assertEquals('Change password success', $result['success']['title']);
        $this->assertEquals('1', $result['success']['message']['user_id']);

        // Login with changed password
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'password_new',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => true,
                            'plain_text'        => false,
                            'md5'               => false,
                            'own_class'         => false
                         ]);

        $result2 = $this->lf->run();
        $this->assertEquals('Login success', $result2['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result2['success']['message']['email']);
    }

    public function testForgottenPasswordChangeAuthPlain() {
        LfConfig::set('_POST',                $this->forgottenPasswordChangeData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => true,
                            'md5'               => false,
                            'own_class'         => false
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Change password success', $result['success']['title']);
        $this->assertEquals('1', $result['success']['message']['user_id']);

        // Login with changed password
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'password_new',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);

        $result2 = $this->lf->run();
        $this->assertEquals('Login success', $result2['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result2['success']['message']['email']);
    }

    public function testForgottenPasswordChangeAuthMD5() {
        LfConfig::set('_POST',                $this->forgottenPasswordChangeData);
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => true,
                            'own_class'         => false
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Change password success', $result['success']['title']);
        $this->assertEquals('1', $result['success']['message']['user_id']);

        // Login with changed password
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'password_new',
                    'type' => 'login'
                ];

        LfConfig::set('_POST',                  $loginData);

        $result2 = $this->lf->run();
        $this->assertEquals('Login success', $result2['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result2['success']['message']['email']);
    }

    public function testForgottenPasswordChangeAuthOwnClass() {
        LfConfig::set('_POST',                $this->forgottenPasswordChangeData);

        // Encode password by custom class
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => false,
                            'md5'               => false,
                            'own_class'         => new CustomPasswordCompare()
                         ]);

        $result = $this->lf->run();
        $this->assertEquals('Change password success', $result['success']['title']);
        $this->assertEquals('1', $result['success']['message']['user_id']);

        // Login with changed password
        $loginData = [
                    'login' => 'test@loginframework.cz',
                    'password' => 'xxpassword_newxx',
                    'type' => 'login'
                ];

        // Decode password by plain text because I know how encoded password looks like
        // Correctly should own class have encode and compare comatible
        LfConfig::set('AUTH_TYPE', [
                            'password_verify'   => false,
                            'plain_text'        => true,
                            'md5'               => false,
                            'own_class'         => false
                         ]);
        LfConfig::set('_POST',                  $loginData);

        $result2 = $this->lf->run();
        $this->assertEquals('Login success', $result2['success']['title']);
        $this->assertEquals('test@loginframework.cz', $result2['success']['message']['email']);
    }

    public function testForgottenPasswordQuestionMarkNotChanged() {
        LfConfig::set('_POST',                $this->forgottenPasswordRequestData);
        LfConfig::set('FORGOTTEN_EMAIL_LINK', 'http://domain.com/something/example.php');

        $result = $this->lf->run();
        $this->assertEquals(1, substr_count($result['success']['dev_message']['link'], '?'));
    }

    public function testForgottenPasswordQuestionMarkChanged() {
        LfConfig::set('_POST',                $this->forgottenPasswordRequestData);
        LfConfig::set('FORGOTTEN_EMAIL_LINK', 'http://domain.com/something/example.php?foo=123');

        $result = $this->lf->run();
        $this->assertEquals(1, substr_count($result['success']['dev_message']['link'], '?'));
    }
}