<?php
class LfTextTest extends PHPUnit_Framework_TestCase {
    public function testGet() {
        $this->assertEquals('Login failed', LfText::get('LoginFailed'));
    }

    public function testGetNotSet() {
        $this->assertEquals('textValueForThisKeyNotSet:NOTSET', LfText::get('NOTSET'));
    }
}
