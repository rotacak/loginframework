<?php
/**
 * Class for easy setup database test case
 */
abstract class DBTestCase extends PHPUnit_Extensions_Database_TestCase {
    /**
     * Only instantiate pdo once for test clean-up/fixture load
     *
     * @var PDO
     */
    protected $pdo = null;

    /**
     * Only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
     *
     * @var
     */
    private $conn = null;

    /**
     * Connect to database in memory and only once
     *
     * @return
     */
    protected function getConnection() {
        if ($this->conn === null) {
            if ($this->pdo == null) {
                $this->pdo = new PDO('mysql:dbname=loginFramework_tests;host=127.0.0.1', 'root', '');
            }
            $this->conn = $this->createDefaultDBConnection($this->pdo, 'loginFramework_tests');
        }

        return $this->conn;
    }

    /**
     * Load data into database
     *
     * @param string $file - Filename with path
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet($file = null) {
        if ($file === null)
            $fileLoad = 'tests/db/loginFramework.xml';
        else
            $fileLoad = $file;

        return $this->createMySQLXMLDataSet($fileLoad);
    }
}