<?php
/*
 *  Load all required files to run tests
 */
require 'LfSrc/LfConfig.php';
require 'LfSrc/LfEmail.php';
require 'LfSrc/LfMessage.php';
require 'LfSrc/LfText.php';
require 'LfSrc/LfHashEquals.php';
require 'LfSrc/LoginFramework.php';

require 'tests/DBTestCase.php';
require 'tests/DefaultLfTestConfig.php';